#include <stdio.h>

int main(int argc, char *argv[]) {
	double a, b, result;
	int n, variante;
	
	if (argc<2) {
		fprintf(stderr, "Numero de argumentos incorrecto\n");
		return 1;
	}
	if (argc>2) n=atoi(argv[2]);
	else n=1000;
	
	a=0;
	b=1;
	variante=atoi(argv[1]);
	switch (variante) {
		case 1:
			result = calcula_integral1(a,b,n);
			break;
		case 2:
			result = calcula_integral2(a,b,n);
			break;
		default:
			fprintf(stderr, "Numero de variante incorrecto\n");
			return 1;
	}
	printf("Valor de la integral = %.12f\n", result);
	return 0;
}
