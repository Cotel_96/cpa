#!/bin/sh
#PBS -q cpa
#PBS -l nodes=1,walltime=00:9:00
#PBS -o comparaHilos.txt
#PBS -d .

OMP_NUM_THREADS=1 ./imagenesP1.o
OMP_NUM_THREADS=2 ./imagenesP1.o
OMP_NUM_THREADS=4 ./imagenesP1.o
OMP_NUM_THREADS=16 ./imagenesP1.o
OMP_NUM_THREADS=32 ./imagenesP1.o

