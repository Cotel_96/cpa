#!/bin/sh
#PBS -q cpa
#PBS -l nodes=1,walltime=00:10:00
#PBS -o comparaParalelos.txt
#PBS -d .

OMP_NUM_THREADS=8 ./imagenesP1.o
OMP_NUM_THREADS=8 ./imagenesP2.o
OMP_NUM_THREADS=8 ./imagenesP3.o
OMP_NUM_THREADS=8 ./imagenesP4.o
