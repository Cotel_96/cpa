#include <stdio.h>
#include <omp.h>

int main() {
	int yo, num_hilos;
	#pragma omp parallel private(yo)
	{
		num_hilos = omp_get_num_threads();
		yo = omp_get_thread_num();
		printf("%d/%d: Hola, mundo\n", yo, num_hilos);
	}
	return 0;
}

