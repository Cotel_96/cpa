#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <omp.h>

typedef unsigned long long Entero_grande;
#define ENTERO_MAS_GRANDE  ULLONG_MAX

int primo(Entero_grande n)
{
  int p;
  Entero_grande i, s;

  p = (n % 2 != 0 || n == 2);

  if (p) {
    s = sqrt(n);
	int hilo, nhilos;
	
	#pragma omp parallel private(hilo, i)
	{
		hilo = omp_get_thread_num();
		nhilos = omp_get_num_threads();
		for (i = 2*hilo+3; p && i <= s; i += 2*nhilos)
			if (n % i == 0) p = 0;
	}
    
  }

  return p;
}

int main()
{
  Entero_grande n;

  for (n = ENTERO_MAS_GRANDE; !primo(n); n -= 2) {
    /* NADA */
  }

  printf("El mayor primo que cabe en %d bytes es %llu.\n",
         sizeof(Entero_grande), n);

  return 0;
}
