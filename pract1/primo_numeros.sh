#!/bin/sh
#PBS -q cpa
#PBS -l nodes=1,walltime=00:10:00
#PBS -o resultado_primo_numerosP.txt
#PBS -d .

OMP_SCHEDULE=dynamic,2 ./primo_numerosP.o
OMP_SCHEDULE=static,3 ./primo_numerosP.o
