#!/bin/sh
#PBS -q cpa
#PBS -l nodes=1,walltime=00:10:00
#PBS -o resultado_tabla.txt
#PBS -d .

OMP_NUM_THREADS=8 ./encaja-e2-pi.o -t
OMP_NUM_THREADS=8 ./encaja-e2-pj.o -t
OMP_NUM_THREADS=8 ./encaja-e2-px.o -t
